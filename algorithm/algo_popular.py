import sys
import pandas as pd
import random
from pubsub import pub
from algo_base import AlgorithmBase

class AlgorithmPopular(AlgorithmBase):
    rec_size = 0;
    pd_temp = []
    item_db = pd.DataFrame({})
    buy_db = pd.DataFrame([],columns = ['ItemId', 'Count']);
    reward_db = pd.DataFrame({})
    rec_db = pd.DataFrame([],columns = ['SessionID', 'Date', 'ItemId', 'Event']);

    def AlgoConfig(self, rsize):
        self.rec_size = int(rsize);

    def EventHandler(self, pd):
        """Event Handler Object"""
        self.pd_temp = pd.copy(deep=True)

        #Check if item is present in the item database
        if (self.item_db.empty == True):
            self.item_db = self.item_db.append(self.pd_temp.drop(['Date', 'SessionID', 'Event'], axis=1, inplace=False));
        elif (self.pd_temp['ItemId'].isin(self.item_db['ItemId']).any() == False):
            self.item_db = self.item_db.append(self.pd_temp.drop(['Date', 'SessionID','Event'], axis=1,inplace=False));
        self.item_db = self.item_db.reset_index(drop=True)

        #Hardcoded for now
        if (pd['Event'].values == 3):

            if((self.buy_db.shape[0] > 0) and (self.buy_db['ItemId'].isin(self.pd_temp['ItemId']).any() == True)):
                index = self.buy_db[self.buy_db['ItemId'].isin(self.pd_temp['ItemId'])].index
                self.buy_db.loc[index, 'Count'] +=1
            else:
                self.pd_temp['Count'] = 1
                self.buy_db = self.buy_db.append(self.pd_temp.drop(['Date', 'SessionID','Event'], axis=1,inplace=False));
        return 
    
    def RequestRecommendation(self):
        """Recommendation Handler Object"""
        print 'Popular'
        topn = pd.DataFrame([],columns = ['SessionID', 'Date', 'ItemId', 'Event']);
        temp = pd.DataFrame([]);
        bsize = 0

        if (int(self.buy_db.shape[0]) >= self.rec_size):
            bsize = self.rec_size
        #    temp = self.buy_db.nlargest(self.rec_size, 'Count', keep='first')
            #This is a bug in pandas when the largest values are duplicate
        #   temp = temp.head(self.rec_size)
        elif (self.buy_db.shape[0] > 0):
#            print 'shape', self.buy_db.shape[0]
            bsize = self.buy_db.shape[0]
#            temp = self.buy_db.nlargest(ts, 'Count', keep='first')
            #This is a bug in pandas when the largest values are duplicate
 #           temp = temp.head(self.buy_db.shape[0])

        temp = self.buy_db.sort_values('Count', ascending=False)[:bsize].sort_index()

        #Get recommendation Items if length is greater than 0
        if (len(temp) > 0):
            topn['ItemId'] = temp['ItemId']

            #Fill in rest of the columns 
            topn = topn.reset_index(drop=True)
            ts = int(self.pd_temp['SessionID'])
            topn['SessionID'] = ts
            ts = int(self.pd_temp['Date'])
            topn['Date'] = ts
            ts = int(self.pd_temp['Event'])
            topn['Event'] = ts 

            #Append the data to the data frame
            self.rec_db.append(topn )
            self.pd_temp = self.pd_temp.iloc[0:0]

        return topn

    def RewardHandler(self, pd):
        print 'Got Reward'
        """Reward Handler Object"""
        return

