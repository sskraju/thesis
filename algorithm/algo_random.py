import sys
import pandas as pd
from pubsub import pub
from algo_base import AlgorithmBase
import random


class AlgorithmRandom(AlgorithmBase):
    rec_size = 0
    pd_temp = []       
    reward_db = pd.DataFrame({})
    item_db = pd.DataFrame({})
    rec_db = pd.DataFrame(pd_temp,columns = ['SessionID', 'Date', 'ItemId', 'Event']);


    def AlgoConfig(self, rsize):
        self.rec_size = int(rsize);

    def EventHandler(self, pd):
        """Event Handler Object"""
        self.pd_temp = pd.copy(deep=True)

        #Check if item is present in the item database
        if (self.item_db.empty == True):
            self.item_db = self.item_db.append(self.pd_temp.drop(['Date', 'SessionID', 'Event'], axis=1, inplace=False));
        elif (self.pd_temp['ItemId'].isin(self.item_db['ItemId']).any() == False):
            self.item_db = self.item_db.append(self.pd_temp.drop(['Date', 'SessionID','Event'], axis=1,inplace=False));
        self.item_db = self.item_db.reset_index(drop=True)
        return 
    
    def RequestRecommendation(self):
        """Recommendation Handler Object"""
        print 'Random'
        # Get rec_size random items from the item database
        if (int(self.item_db.shape[0]) >  self.rec_size) :
            item_locations = random.sample(range(0,self.item_db.shape[0]-1),self.rec_size);
        else:
            item_locations = random.sample(range(0,self.item_db.shape[0]),self.item_db.shape[0]);

        #Get recommendation Items
        temp_recs = pd.DataFrame([],columns = ['SessionID', 'Date', 'ItemId', 'Event']);
        temp_recs['ItemId'] = self.item_db.ix[item_locations]['ItemId']

    
        #Fill in rest of the columns 
        temp_recs = temp_recs.reset_index(drop=True)
        ts = int(self.pd_temp['SessionID'])
        temp_recs['SessionID'] = ts
        ts = int(self.pd_temp['Date'])
        temp_recs['Date'] = ts
        ts = int(self.pd_temp['Event'])
        temp_recs['Event'] = ts 

        #Append the data to the data frame
        self.rec_db.append(temp_recs)
        self.pd_temp = self.pd_temp.iloc[0:0]

        return temp_recs;
    
    def RewardHandler(self, pd):
        """Reward Handler Object"""
        reward_db.append(pd);
