import numpy
import sys
import time
import pandas as pd
import abc
from pubsub import pub

class MetricBase(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def InitMetric(self, num_algos):
        """Init Metrics"""
        return

    @abc.abstractmethod
    def UpdateMetric(self, rec_tmp, cdata):
        """Update Metrics"""
        return
