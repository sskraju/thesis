from __future__ import division
import numpy as np
import sys
import time
import pandas as pd
import abc
from metric_base import MetricBase
from matplotlib.ticker import MaxNLocator
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.animation as animation
import time
from copy import deepcopy

class MetricCTR(MetricBase):
    total_list = [];
    cnt1 = 0
    cnt2 = 0
    ilen = []
    ratio = []
    p1 = []
    legend = 0
    num_algos = 0
    def InitMetric(self, num_algos):
        global_lists = [];
        self.total_list = pd.DataFrame(global_lists,columns = ['Algo', 'RLen', 'CLen', 'ILen']);
        plt.ion()
        plt.show()
        plt.figure(1)
        self.num_algos = num_algos
        self.cnt1 = 1
        self.cnt2 = 1
        self.legend = plt.legend(title="title", loc=4, fontsize='small', fancybox=True)

        tmp_lst = pd.DataFrame({'Algo':pd.Series([], dtype='str'),
                   'RLen':pd.Series([], dtype='float'),
                   'CLen':pd.Series([], dtype='float'),
                   'ILen':pd.Series([], dtype='float')})

        for i in range(1, num_algos+1):
            self.ilen.append([])
            self.ratio.append([])
            #Create a dataframe
            ts = pd.Series(i-1)
            tmp_lst['Algo'] = ts
            ts = 0
            tmp_lst['RLen'] = ts
            ts = 0
            tmp_lst['CLen'] = ts
            ts = 0
            tmp_lst['ILen'] = ts 
            self.total_list = self.total_list.append(tmp_lst);
            tmp_lst=tmp_lst.iloc[0:0]
            self.total_list = self.total_list.reset_index(drop=True)

    def UpdateMetric(self, rec_tmp, cdata):
        """Event Handler Object"""

        tmp_lsts = []
        tmp_lst = pd.DataFrame({'Algo':pd.Series([], dtype='str'),
                   'RLen':pd.Series([], dtype='float'),
                   'CLen':pd.Series([], dtype='float'),
                   'ILen':pd.Series([], dtype='float')})
#        tmp_lst = pd.DataFrame(tmp_lsts,columns = ['Algo','RLen','CLen', 'ILen'], dtype=[np.str, np.int64,np.int64,np.int64]);
        count = 0
        cdata = cdata

        # Hardcoded event
        if ((cdata['Event'] == 3).any() == True):
            return
#        cdata.drop(cdata.index[cdata['Event'] == 3].tolist())
       # if (int_data.loc[int_data['Event'] == 3].empty == False):
        #    data_set.drop(int_data.index, 0, inplace=True)

        #loop through unique algorithms
        for algo in rec_tmp.Algo.unique():
            algo_rec = rec_tmp.loc[rec_tmp['Algo'] == algo]
            if(len(cdata['ItemId'].isin(algo_rec['ItemId'])) > 0):
                #Create a dataframe
                ts = pd.Series(algo)
                tmp_lst['Algo'] = ts
                ts = pd.Series(algo_rec.shape[0])
                tmp_lst['RLen'] = ts
                ts = 1
                tmp_lst['CLen'] = ts
                ts = pd.Series(cdata.shape[0])
                tmp_lst['ILen'] = ts

                print 'CCCCCCCCCCCCCCCCCCCCCCCCCS'
                if (len(self.total_list) == 0):
                    self.total_list =  tmp_lst.copy();
                elif(tmp_lst['Algo'].isin(self.total_list['Algo']).any() == False):
                    self.total_list = self.total_list.append(tmp_lst);
                    self.total_list = self.total_list.reset_index(drop=True)
                else:
                    idx = np.where(self.total_list['Algo'] == algo)
                    print' idx ctr', idx[0]
                    self.total_list.loc[idx[0], 'RLen'] += tmp_lst.iloc[0]['RLen']
                    self.total_list.loc[idx[0], 'CLen'] += tmp_lst.iloc[0]['CLen']
            else:
                idx = np.where(self.total_list['Algo'] == algo)
                ts = pd.Series(algo_rec.shape[0])
                tmp_lst['RLen'] = ts
                self.total_list.loc[idx[0], 'RLen'] += tmp_lst.iloc[0]['RLen']

            tmp_lst=tmp_lst.iloc[0:0]
            print self.total_list
            print 'CCCCCCCCCCCCCCCCCCCCCCCCCT'

        for count in range(0, self.num_algos):
            self.total_list = self.total_list.reset_index(drop=True)
            tst1 = self.total_list.iloc[count]['CLen'].copy()
            tst2  = self.total_list.iloc[count]['RLen'].copy()
            self.total_list.loc[count]['ILen'] += 1
            if tst1 == 0:
                tmp_ratio = 0
            else:
                tmp_ratio = tst1/tst2
            self.ilen[count].append(self.total_list.iloc[count]['ILen'].copy())
            self.ratio[count].append(tmp_ratio)
            count = count+1

    #        self.total_list.plot(tst1, tst2, color='red')
    #        plt.scatter(self.total_list.iloc[0]['ILen'], tmp_ratio, color='red')
#            self.p1[0].set_xdata(np.append(self.p1[0].get_xdata(), self.total_list.iloc[0]['ILen']))
#            self.p1[0].set_ydata(np.append(self.p1[0].get_ydata(), tmp_ratio))
        plt.figure(1)
        tt = plt.plot(self.ilen[0], self.ratio[0], color = 'red')
        tt2 =  plt.plot(self.ilen[1], self.ratio[1], color = 'Blue')
        self.legend = plt.legend(['Popular', 'Random'])
#            self.p1[count].xlabel('Items')
#            self.p1[count].ylabel('Click Through Rate')
#            self.p1[count].title('Click Through rate for Algorithms')
        plt.ylim(0,1)
        plt.xlabel('Queries')
        plt.ylabel('Click Through Rate')
        plt.title('Click Through rate for Algorithms')
    #        plt.xticks(self.total_list['ILen'])
        plt.draw()
#            self.p1[count].draw()
        plt.pause(0.0001)
#            plt.show()
#            plt.pause(0.0001)

        return
