import numpy
import sys
import time
import pandas as pd
from pubsub import pub
sys.path.insert(0, './algorithm')
sys.path.insert(0, './config_files')
sys.path.insert(0, './data')
sys.path.insert(0, './evaluator')
sys.path.insert(0, './main')
sys.path.insert(0, './metrics')
sys.path.insert(0, './plot')
from parse_config import parse_config
from metric_calculator import *
from algorithm_handler import TriggerCallback
from read_data import getData,initData
from algo_random import *
from algo_popular import *
from metric_ctr import *
from metric_precision import *
from bokeh.plotting import figure, output_file, show
#from popular import *

class Evaluator:
    recset = 0;
    cfg_metrics = 0;
    recsize = 0;
    item_db = 0;
    hitset = 0;
    cfg_algos = 0;
    algorithms = [];
    metrics = [];
    reco_db = pd.DataFrame({})

    # Read data from configuration files
    def __init__(self, algos, metrics, FilePath):
        print ("Initializing Evaluator...");

        self.hitset, self.recset, self.cfg_metrics, self.recsize, self.cfg_algos = parse_config(FilePath);

        #convert config into array
        self.hitset =  map(int, self.hitset.rstrip(',').split(','))
        self.recset =  map(int, self.recset.rstrip(',').split(','))
        self.cfg_metrics =  self.cfg_metrics.rstrip(',').split(',')
        self.cfg_algos   =   self.cfg_algos.rstrip(',').split(',')

        self.algorithms = algos;
        self.metrics    = metrics

        #Init Data
        print ("Initializing Dataset...");
        self.item_db = initData(self.hitset);
        
        #Initialize metric caluclators
        for i in self.metrics:
            i.InitMetric(len(self.algorithms))

        #Call Algorithm Init
        for i in self.algorithms:
            i.AlgoConfig(self.recsize)

        print ("Initialization Complete...")

        
    
    def start_evaluator(self):
        print ("Starting Evaluator...");

        rec_tmp = pd.DataFrame({})
        #Read Data
        gd = getData();
        #Loop until nothing else to read
        while (gd.empty == False):
            count = 0
            #Call Algorithm and get Recommendations
            for i in self.algorithms:
                # Call the algorithm with event handler
                i.EventHandler(gd);

                #Request recommendation
                tmp = i.RequestRecommendation();
                if (len(tmp) > 0):
                    tmp['Algo'] = count;
                    if rec_tmp.empty:
                        rec_tmp = tmp;
                    else:
                        rec_tmp = rec_tmp.append(tmp);
                
                count += 1
       
            self.reco_db = self.reco_db.append(rec_tmp)
       #     print rec_tmp
            #Read Data Again
            gd = getData();
       #     print gd
        
            count = 0
                #Calculate and plot metrics

            for i in self.metrics:
                #print rec_tmp
                i.UpdateMetric(rec_tmp, gd)

            # Delete the tmp
            rec_tmp = rec_tmp.iloc[0:0]
        print ('Evaluator Run Complete')


    def Reward_Calculator(self):
    # If the session and item matches a recommendation
        print 'Hello'
            # then provide a reward            
#            for i in self.algorithms:
#                if ((len(self.reco_db_list[count]) != 0) and (gd.loc[gd['SessionID'].isin(self.reco_db_list[count].iloc[:,0])]['Event'].values == 3 )):
        
                    #Generate Reward
       #             i.RewardHandler(gd)
        
        #        if ((len(self.reco_db_list[count]) != 0)):
         #           self.reco_db_list[count].drop(self.reco_db_list[count].index[0], inplace=True)



    def stop_evaluator(self):
        print ("Stopping Evaluator");

